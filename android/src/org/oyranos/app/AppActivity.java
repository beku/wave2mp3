package org.oyranos.app;

import android.app.Activity;
import android.os.Bundle;
import android.os.Build;
import android.os.Environment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.net.Uri;
import java.io.*;
import java.lang.Object;
import java.util.HashMap;
import java.util.Iterator;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;


public class AppActivity extends QtActivity {

    private static int debug = 0;

    // during first activity call in a task
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onCreateBundle() [" + getTaskId() + "]" );

        super.onCreate(savedInstanceState);

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onCreateBundle()" );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onActivityResult request: " + requestCode + " result: " + resultCode );

        super.onActivityResult( requestCode, resultCode, data );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onActivityResult()" );
    }

    @Override
    protected void onNewIntent (Intent intent)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onNewIntent()" );

        super.onNewIntent( intent );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onNewIntent()" );
    }

    @Override
    protected void onPause ()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onPause()" );

        super.onPause( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onPause()" );
    }

    @Override
    protected void onResume ()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onResume()" );

        super.onResume( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onResume()" );
    }

    @Override
    protected void onStart()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onStart()" );

        super.onStart( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onStart()" );
    }

    @Override
    protected void onRestart()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onRestart()" );

        super.onRestart( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onRestart()" );
    }

    @Override
    protected void onStop()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onStop()" );

        super.onStop( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onStop()" );
    }

    @Override
    protected void onDestroy()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onDestroy()" );

        super.onDestroy( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onDestroy()" );
    }
}

