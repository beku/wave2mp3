/** @file wave2mp3.c
 *
 *  wave2mp3 is for WindTone.
 *
 *  @par Copyright:
 *            2019-2023 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2019/12/03
 *
 *  simple Wave -> MP3 converter
 */

// cc -Wall -Wextra -g -I $HOME/.local/include/lame/ -I /home/kuwe/.local/include/oyjl/ -I src wave2mp3.c -o wave2mp3 -L=$HOME/.local/lib64 -lmp3lame-static -lOyjlCore -ldl -lm
// cc -Wall -Wextra -I=$HOME/.local/include/lame/ wave2mp3.c -o wave2mp3 -L=$HOME/.local/lib64 -lmp3lame -lOyjlCore -ldl -lm

#include <stdio.h>
#include <lame.h>
#include <oyjl.h>
#define WAVE_API_SKIP_MAIN

#include "oyjl_version.h"

extern char **environ;
#ifdef OYJL_HAVE_LOCALE_H
#include <locale.h>
#endif
#define MY_DOMAIN "wave2mp3"
oyjlTranslation_s * trc = NULL;
# ifdef _
# undef _
# endif
# define _(text) oyjlTranslate( trc, text )

#include "wave-api.c"

void my_debugf(const char *format, va_list ap)
{
  (void) vfprintf(stdout, format, ap);
}

int oyjlLowerStrcmpWrap_ (const void * a_, const void * b_)
{
  const char * a = *(const char **)a_,
             * b = *(const char **)b_;
#ifdef HAVE_POSIX
  return strcasecmp(a,b);
#else
  return strcmp(a,b);
#endif
}

int oyjlLowerStrcmpInverseWrap_ (const void * a_, const void * b_)
{
  const char * a = *(const char **)b_,
             * b = *(const char **)a_;
#ifdef HAVE_POSIX
  return strcasecmp(a,b);
#else
  return strcmp(a,b);
#endif
}

static char ** environment = NULL;
static char * xdg_user_dir_lookup (const char *type);
#include <dirent.h>
void listFiles(const char * path, char *** list, int * count, const char * suffix)

{
  DIR * d = opendir(path);
  if(d == NULL) return;
  struct dirent * dir;
  while ((dir = readdir(d)) != NULL)
    {
      if(dir->d_type != DT_DIR)
      {
        char * name = NULL; oyjlStringAdd( &name, 0,0, "%s/%s", path, dir->d_name);
        if(!suffix)
            oyjlStringListPush( list, count, name, malloc, free );
        else
        {
          char * dot = strrchr( name, '.' ),
               * end = dot ? dot+1 : NULL;
          if(end && strcasecmp(end, suffix) == 0)
            oyjlStringListPush( list, count, name, malloc, free );
        }
        free(name);
      }
      else
      if( dir->d_type == DT_DIR && strcmp(dir->d_name,".") != 0 &&
          strcmp(dir->d_name,"..") != 0 )
      {
        char * d_path = NULL; oyjlStringAdd( &d_path, 0,0, "%s/%s", path, dir->d_name);
        listFiles(d_path, list, count, suffix);
        free(d_path);
      }
    }

    closedir(d);
}

oyjlOptionChoice_s * getWaveChoices             ( oyjlOption_s      * o OYJL_UNUSED,
                                                  int               * selected,
                                                  oyjlOptions_s     * opts OYJL_UNUSED )
{
    int choices = 0, current = -1;
    char ** choices_string_list = NULL;
    int error = 0;

    if(!error)
    {
      int i;
      const char * man_page = getenv("DISPLAY");
      int skip_real_info = man_page && strcmp(man_page,"man_page") == 0;
      if(!skip_real_info)
      {
        const char * xdg =
  #ifdef __ANDROID__
                  "/storage/emulated/0/Music";
  #else
                  xdg_user_dir_lookup( "Music" );
  #endif
        //printf("music: %s\n", xdg);
        listFiles(xdg, &choices_string_list, &choices, "wav");
        qsort( choices_string_list, (size_t)choices, sizeof(char*), oyjlLowerStrcmpInverseWrap_ );

      }
      oyjlOptionChoice_s * c = calloc((unsigned int)choices+1, sizeof(oyjlOptionChoice_s));
      if(c)
      {
        for(i = 0; i < choices; ++i)
        {
          char * v = malloc(12);

          sprintf(v, "%d", i);
          c[i].nick = strdup(choices_string_list[i]);
          c[i].name = strdup(choices_string_list[i]);
          c[i].description = strdup("");
          c[i].help = strdup("");
        }
        c[i].nick = malloc(4);
        c[i].nick[0] = '\000';
      }
      oyjlStringListRelease( &choices_string_list, choices, free );
      if(selected)
        *selected = current;

      return c;
    } else
      return NULL;
}

/* This function is called the
 * * first time for GUI generation and then
 * * for executing the tool.
 */
int myMain(int argc, const char**argv)
{
  const char * output = NULL;
  const char * file = NULL;
  int info = 0;
  const char * render = 0;
  int help = 0;
  int verbose = 0;
  int version = 0;
  const char * exportX = NULL;
  const char * export = 0;
  int state = 0;
  int error = 0;

  /* handle options */
  /* Select from *version*, *manufacturer*, *copyright*, *license*, *url*,
   * *support*, *download*, *sources*, *oyjl_modules_author* and
   * *documentation* what you see fit. Add new ones as needed. */
  oyjlUiHeaderSection_s sections[] = {
    /* type, nick,            label, name,                  description  */
    {"oihs", "version",       NULL,  "1.0",                 NULL},
    {"oihs", "manufacturer",  NULL,  "The LAME Project",    "https://lame.sourceforge.io/"},
    {"oihs", "copyright",     NULL,  "Copyright © 2019-2023 Kai-Uwe Behrmann", NULL },
    {"oihs", "license",       NULL,  "AGPL-3.0", "https://opensource.org/licenses/AGPL-3.0" },
    {"oihs", "oyjl_module_author", NULL, "Kai-Uwe Behrmann","https://www.behrmann.name/"},
    {"oihs", "documentation", NULL,  NULL,                  _("The tool converts wave audio files to mp3. The Lame API is used to encode the audio.")},
    {"oihs", "date",          NULL,  "2021-08-23T12:00:00", _("August 23, 2021")},
    /* use newline separated permissions in name + write newline separated list in description; both lists need to match in count */
    {"oihs", "permissions",   NULL,  "android.permission.READ_EXTERNAL_STORAGE\nandroid.permission.WRITE_EXTERNAL_STORAGE", _("Read external storage for global data access, like downloads, music ...\nWrite external storage to create and modify global data.")},
    {"",0,0,0,0}};

  /* declare options - the core information; use previously declared choices */
  oyjlOption_s oarray[] = {
  /* type,   flags, o,   option,    key,  name,         description,         help, value_name,    value_type,               values,                                                          variable_type, output variable */
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE,     "i", "input",   NULL, _("input"),   _("Set Input"),      _("Wave Audio File"), _("FILENAME"), oyjlOPTIONTYPE_FUNCTION, {.getChoices = getWaveChoices}, oyjlSTRING, {.s=&file}, 0 },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE,     "o", "output",  NULL, _("output"),  _("Control Output"), _("A custom path for the MP3 file."), _("FILENAME"), oyjlOPTIONTYPE_CHOICE, {.choices.list = NULL}, oyjlSTRING, {.s = &output}, 0 },
    {"oiwi", 0,     "I", "info",    NULL, _("Info"),    _("Information"),    _("Show only infos, do not encode"), NULL,          oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i = &info}, 0 },
    /* default options -h and -v */
    {"oiwi", OYJL_OPTION_FLAG_ACCEPT_NO_ARG, "h", "help",    NULL, NULL,    NULL,           NULL, NULL,          oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i = &help}, 0 },
    {"oiwi", 0, NULL, "synopsis", NULL, NULL, NULL,      NULL,     NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlNONE, {0}, 0 },
    {"oiwi", 0,     "v", "verbose", NULL, _("Verbose"), _("Verbose"),        NULL, NULL,          oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i = &verbose}, 0 },
    /* The --render option can be hidden and used only internally. */
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_MAINTENANCE, "R","render",NULL,NULL,NULL,NULL,NULL,oyjlOPTIONTYPE_CHOICE,{0},oyjlSTRING,{.s=&render}, 0},
    {"oiwi", 0, "V", "version", NULL, _("Version"), _("Version"), NULL, NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i=&version}, 0 },
    /* default option template -X|--export */
    {"oiwi", 0,     "X", "export",  NULL, NULL,         NULL,                NULL, NULL,          oyjlOPTIONTYPE_CHOICE, {0}, oyjlSTRING, {.s = &exportX}, 0 },
    {"",0,0,0,0,0,0,0, NULL, oyjlOPTIONTYPE_END, {0},0,{0},0}
  };

  /* declare option groups, for better syntax checking and UI groups */
  oyjlOptionGroup_s groups_no_args[] = {
  /* type,   flags, name,      description,          help, mandatory, optional, detail */
    {"oiwg", 0,     _("Mode1"),_("Simple mode"),     NULL, "i",       "o,v,I",  "i,o,I", 0}, /* accepted even if none of the mandatory options is set */
    {"oiwg", 0,     _("Misc"), _("General options"), NULL, "h|X|V|R", "v",      "h,X,V,R,v", 0}, /* just show in documentation */
    {"",0,0,0,0,0,0,0,0}
  };

  /* tell about the tool */
  oyjlUi_s * ui = oyjlUi_Create( argc, argv, /* argc+argv are required for parsing the command line options */
                                       "wave2mp3", _("Lame MP3 Encoder"), _("Convert Wave to mp3"),
#ifdef __ANDROID__
                                       ":/images/logo.svg", // use qrc
#else
                                       "wave2mp3",
#endif
                                       sections, oarray, groups_no_args, &state );
  /* done with options handling */
  if( state & oyjlUI_STATE_EXPORT &&
      export &&
      strcmp(export,"json+command") != 0)
    goto clean_main;
  if(state & oyjlUI_STATE_HELP)
  {
    fprintf( stderr, "%s\n\tman wave2mp3\n\n", _("For more information read the man page:") );
    goto clean_main;
  }

  static int main_count = 0;
  if(verbose)
  {
    ++main_count;
    fprintf(stderr, "started %s %d\n", __func__, main_count );
    char * json = oyjlOptions_ResultsToJson( ui->opts, 0 );
    if(json)
      fputs( json, stderr );
    fputs( "\n", stderr );

    char * text = oyjlOptions_ResultsToText( ui->opts );
    if(text)
      fputs( text, stderr );
    fputs( "\n", stderr );
  }

  if((export && strcmp(export,"json+command") == 0))
  {
    char * json = oyjlUi_ToJson( ui, 0 ),
         * json_commands = NULL;
    oyjlStringAdd( &json_commands, malloc, free, "{\n  \"command_set\": \"%s\",", argv[0] );
    oyjlStringAdd( &json_commands, malloc, free, "%s", &json[1] ); /* skip opening '{' */
    puts( json_commands );
    goto clean_main;
  }

  /* GUI boilerplate */
  if(ui && render)
  {
#if !defined(NO_OYJL_ARGS_QML_START)
    int debug = verbose;
    oyjlArgsRender( argc, argv, NULL,NULL,NULL, debug, ui, myMain );
#else
    fprintf( stderr, "No gui support compiled in. For a GUI use -X json and load into oyjl-args-qml viewer." );
#endif
  }
  else if(ui)
  {
    char * tmp = NULL;
    if(verbose)
    {
        /* get the results and do something with it */
        char * result = oyjlOptions_ResultsToJson(ui->opts, 0);
        fprintf(stderr, "%s\n", result);
        free(result);
    }

    if(output)
        fprintf( stderr, "output:\t%s\n", output );

    if(file)
    {
      if(!output)
      {
        oyjlStringAdd( &tmp, 0,0, "%s", file );
        char * t = &tmp[strlen(file)-3];
        t[0] = 0;
        oyjlStringAdd( &tmp, 0,0, "mp3" );
        output = tmp;
      }

      fprintf( stderr, "%s:\t\t%s\n", _("File"), file );

      FILE * fp = fopen(file,"r");
      RiffWave_t * wave = NULL;
      size_t r;

      if(!fp)
      {
        fprintf( stderr, "Could not open %s\n", argv[1] );
        return 1;
      }

      char block[1024];
      memset(block,0,1024);
      wave = (RiffWave_t*) &block;
      r = fread( &block, sizeof(char), 1024, fp );
      if(r > 44 && !isWave(wave))
        puts("no Wave file");

      printf( "%s: %s\n", printWaveHeader( wave ), argv[1]?argv[1]:"---" );
      if(info)
        return 0;
      int num_samples = (int)wave->data_size / (wave->data_bits / 8) / wave->channels;
      printf( "%d samples\n", num_samples );
      char * pcm = malloc(wave->data_size);
      if(!pcm)
        printf( "Could not allocate %d bytes of memory\n", wave->data_size );
      size_t offset = getWaveOffset( wave, 0.0 );
      fseek(fp, (long)offset, SEEK_SET);
      r = fread( pcm, sizeof(char), wave->data_size, fp );

      lame_global_flags *gfp;
      gfp = lame_init();

      lame_set_errorf(gfp,my_debugf);
      lame_set_debugf(gfp,my_debugf);
      lame_set_msgf(gfp,my_debugf);
      lame_set_num_channels(gfp, (int)wave->channels);
      lame_set_in_samplerate(gfp, (int)wave->sample_rate);
      //lame_set_brate(gfp,128);
      //lame_set_mode(gfp,1);
      if(wave->channels == 1)
        lame_set_mode(gfp, MONO);
      //lame_set_quality(gfp,2);   /* 2=high  5 = medium  7=low */ 
      lame_set_VBR( gfp, vbr_mtrh );
      lame_set_VBR_quality( gfp, 0 );
      lame_set_VBR_max_bitrate_kbps( gfp, 320 );
      lame_set_VBR_mean_bitrate_kbps( gfp, 256 );
      lame_set_VBR_min_bitrate_kbps( gfp, 64 );

      const char * lv = get_lame_version();
      printf("Lame v%s\n", lv);
      int ret_code = lame_init_params(gfp);
      if(ret_code)
      {
        fprintf(stderr, "Error while init lame: %d\n", ret_code);
        return 1;
      }
      char * mp3buf = malloc(wave->data_size);
      memset( mp3buf, 0, wave->data_size );
      int result = 0;
      if(wave->channels == 1)
        result = lame_encode_buffer (
                            gfp,           /* global context handle         */
        (const short int*)  pcm,           /* PCM data for left channel     */
                            NULL, //buffer_r [],   /* PCM data for right channel    */
                            num_samples,      /* number of samples per channel */
        (unsigned char*)    mp3buf,        /* pointer to encoded MP3 stream */
        (const int)         wave->data_size ); /* number of valid octets in this
                                              stream                        */
      else
        result = lame_encode_buffer_interleaved(
                            gfp,           /* global context handlei        */
                    (int16_t*)pcm,         /* PCM data for left and right
                                              channel, interleaved          */
                            num_samples,   /* number of samples per channel,
                                              _not_ number of samples in
                                              pcm[]                         */
                  (uint8_t*)mp3buf,        /* pointer to encoded MP3 stream */
                  (int)wave->data_size );  /* number of valid octets in this */
      if(result <= 0)
      {
        fprintf(stderr, "Error while encoding: %d\n", result);
        return 1;
      }

      ret_code = lame_encode_flush(gfp, (uint8_t*)mp3buf, wave->data_size );
      if(ret_code <= 0)
      {
        fprintf(stderr, "Error while flush encoding: %d\n", ret_code);
        return 1;
      } else
        result += ret_code;
      fclose(fp);

      fp = fopen(output,"w+");
      if(!fp)
      {
        printf("Can not write to: %s\n", output);
        return 1;
      }
      r = fwrite( mp3buf, sizeof(char), result, fp );
      if(r != (size_t)result)
      {
        fprintf(stderr, "Error while writing to: %s %lu/%d\n", output, r, result);
        return 1;
      }
      else
        fprintf(stdout, "Wrote to: %s %d bytes\n", output, result);

      lame_mp3_tags_fid( gfp, fp ); /* add VBR tags to mp3 file */

      lame_close( gfp ); 
      fclose(fp);
      free( pcm );
      free( mp3buf );
    }
    if(tmp) free(tmp);
  }
  else
    error = 1;

  oyjlUi_Release( &ui);

  clean_main:
  {
    int i = 0;
    while(oarray[i].type[0])
    {
      if(oarray[i].value_type == oyjlOPTIONTYPE_CHOICE && oarray[i].values.choices.list)
        free(oarray[i].values.choices.list);
      ++i;
    }
  }

  return error;
}

extern int * oyjl_debug;
int main( int argc_, char ** argv_, char ** envv )
{
  int argc = argc_;
  char ** argv = argv_;
  oyjlTranslation_s * trc_ = NULL;
  const char * loc = NULL;
  const char * lang = getenv("LANG");

  /* language needs to be initialised before setup of data structures */
#ifdef __ANDROID__
  setenv("COLORTERM", "1", 0); /* show rich text format on non GNU color extension environment */

  argv = calloc( argc + 2, sizeof(char*) );
  memcpy( argv, argv_, (argc + 2) * sizeof(char*) );
  argv[argc++] = "--render=gui"; /* start QML */
  environment = environ;
#else
  environment = envv;
#endif

  int use_gettext = 0;
#ifdef OYJL_USE_GETTEXT
  //use_gettext = 1;
#endif
#ifdef OYJL_HAVE_LOCALE_H
  loc = setlocale(LC_ALL,"");
#endif
  if(!loc)
  {
    loc = lang;
    fprintf( stderr, "%s", oyjlTermColor(oyjlRED,"Usage Error:") );
    fprintf( stderr, " Environment variable possibly not correct. Translations might fail - LANG=%s\n", oyjlTermColor(oyjlBOLD,lang) );
  }
  if(lang)
    loc = lang;
  if(loc)
  {
    const char * my_domain = MY_DOMAIN;
# include "wave2mp3.i18n.h"
    int size = sizeof(wave2mp3_i18n_oiJS);
    oyjl_val static_catalog = (oyjl_val) oyjlStringAppendN( NULL, (const char*) wave2mp3_i18n_oiJS, size, malloc );
    if(my_domain && strcmp(my_domain,"oyjl") == 0)
      my_domain = NULL;
    trc = trc_ = oyjlTranslation_New( loc, my_domain, &static_catalog, 0,0,0,0 );
  }
  oyjlInitLanguageDebug( "Oyjl", "OYJL_DEBUG", oyjl_debug, use_gettext, "OYJL_LOCALEDIR", OYJL_LOCALEDIR, &trc_, NULL );
  if(MY_DOMAIN && strcmp(MY_DOMAIN,"oyjl") == 0)
    trc = oyjlTranslation_Get( MY_DOMAIN );

  myMain(argc, (const char **)argv);

  oyjlTranslation_Release( &trc_ );
  oyjlLibRelease();

#ifdef __ANDROID__
  free( argv );
#endif

  return 0;
}


/*--------------8<------------------- xdg-user-dir-lookup.c ---------*/
/*
  This file is not licenced under the GPL like the rest of the code.
  Its is under the MIT license, to encourage reuse by cut-and-paste.

  Copyright (c) 2007 Red Hat, Inc.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions: 

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software. 

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * xdg_user_dir_lookup_with_fallback:
 * @type: a string specifying the type of directory
 * @fallback: value to use if the directory isn't specified by the user
 * @returns: a newly allocated absolute pathname
 *
 * Looks up a XDG user directory of the specified type.
 * Example of types are "DESKTOP" and "DOWNLOAD".
 *
 * In case the user hasn't specified any directory for the specified
 * type the value returned is @fallback.
 *
 * The return value is newly allocated and must be freed with
 * free(). The return value is never NULL if @fallback != NULL, unless
 * out of memory.
 **/
static char *
xdg_user_dir_lookup_with_fallback (const char *type, const char *fallback)
{
  FILE *file;
  char *home_dir, *config_home, *config_file;
  char buffer[512];
  char *user_dir;
  char *p, *d;
  int len;
  int relative;
  
  home_dir = getenv ("HOME");

  if (home_dir == NULL)
    goto error;

  config_home = getenv ("XDG_CONFIG_HOME");
  if (config_home == NULL || config_home[0] == 0)
    {
      config_file = (char*) malloc (strlen (home_dir) + strlen ("/.config/user-dirs.dirs") + 1);
      if (config_file == NULL)
        goto error;

      strcpy (config_file, home_dir);
      strcat (config_file, "/.config/user-dirs.dirs");
    }
  else
    {
      config_file = (char*) malloc (strlen (config_home) + strlen ("/user-dirs.dirs") + 1);
      if (config_file == NULL)
        goto error;

      strcpy (config_file, config_home);
      strcat (config_file, "/user-dirs.dirs");
    }

  file = fopen (config_file, "r");
  free (config_file);
  if (file == NULL)
    goto error;

  user_dir = NULL;
  while (fgets (buffer, sizeof (buffer), file))
    {
      /* Remove newline at end */
      len = strlen (buffer);
      if (len > 0 && buffer[len-1] == '\n')
	buffer[len-1] = 0;
      
      p = buffer;
      while (*p == ' ' || *p == '\t')
	p++;
      
      if (strncmp (p, "XDG_", 4) != 0)
	continue;
      p += 4;
      if (strncasecmp (p, type, strlen (type)) != 0)
	continue;
      p += strlen (type);
      if (strncmp (p, "_DIR", 4) != 0)
	continue;
      p += 4;

      while (*p == ' ' || *p == '\t')
	p++;

      if (*p != '=')
	continue;
      p++;
      
      while (*p == ' ' || *p == '\t')
	p++;

      if (*p != '"')
	continue;
      p++;
      
      relative = 0;
      if (strncmp (p, "$HOME/", 6) == 0)
	{
	  p += 6;
	  relative = 1;
	}
      else if (*p != '/')
	continue;
      
      if (relative)
	{
	  user_dir = (char*) malloc (strlen (home_dir) + 1 + strlen (p) + 1);
          if (user_dir == NULL)
            goto error2;

	  strcpy (user_dir, home_dir);
	  strcat (user_dir, "/");
	}
      else
	{
	  user_dir = (char*) malloc (strlen (p) + 1);
          if (user_dir == NULL)
            goto error2;

	  *user_dir = 0;
	}
      
      d = user_dir + strlen (user_dir);
      while (*p && *p != '"')
	{
	  if ((*p == '\\') && (*(p+1) != 0))
	    p++;
	  *d++ = *p++;
	}
      *d = 0;
    }
error2:
  fclose (file);

  if (user_dir)
    return user_dir;

 error:
  if (fallback)
    return strdup (fallback);
  return NULL;
}

/**
 * xdg_user_dir_lookup:
 * @type: a string specifying the type of directory
 * @returns: a newly allocated absolute pathname
 *
 * Looks up a XDG user directory of the specified type.
 * Example of types are "DESKTOP" and "DOWNLOAD".
 *
 * The return value is always != NULL (unless out of memory),
 * and if a directory
 * for the type is not specified by the user the default
 * is the home directory. Except for DESKTOP which defaults
 * to ~/Desktop.
 *
 * The return value is newly allocated and must be freed with
 * free().
 **/
static char *
xdg_user_dir_lookup (const char *type)
{
  char *dir, *home_dir, *user_dir;
	  
  dir = xdg_user_dir_lookup_with_fallback (type, NULL);
  if (dir != NULL)
    return dir;
  
  home_dir = getenv ("HOME");
  
  if (home_dir == NULL)
    return strdup ("/tmp");
  
  /* Special case desktop for historical compatibility */
  if (strcmp (type, "DESKTOP") == 0)
    {
      user_dir = (char*) malloc (strlen (home_dir) + strlen ("/Desktop") + 1);
      if (user_dir == NULL)
        return NULL;

      strcpy (user_dir, home_dir);
      strcat (user_dir, "/Desktop");
      return user_dir;
    }
  
  return strdup (home_dir);
}

#ifdef STANDALONE_XDG_USER_DIR_LOOKUP
int
main (int argc, char *argv[])
{
  if (argc != 2)
    {
      fprintf (stderr, "Usage %s <dir-type>\n", argv[0]);
      exit (1);
    }
  
  printf ("%s\n", xdg_user_dir_lookup (argv[1]));
  return 0;
}
#endif
/*--------------8<------------------- xdg-user-dir-lookup.c ---------*/
