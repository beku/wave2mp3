wave2mp3 README
===============
[![Pipeline](https://gitlab.com/beku/wave2mp3/badges/master/pipeline.svg)](https://gitlab.com/beku/wave2mp3/-/pipelines)
[![Documentation](images/tool-documented.svg)](docs/md/wave2mp3.md)
[![License: AGPL v3](images/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

![](images/logo.svg)

The tool converts wave audio files to mp3. The Lame API is used to encode the audio.

Usage
-----
* [wave2mp3 Tool Syntax](docs/md/wave2mp3.md) [de](docs/md/wave2mp3de.md)


Dependencies
------------
### Mandatory
* [The LAME Project](https://lame.sourceforge.io/) - MP3 encoder
* [Oyjl](https://gitlab.com/beku/oyjl) - Oyjl Shared Basics for CLI parsing, documentation, translation and additional GUIs

Building
--------
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ make install

### Build Flags
... are typical cmake flags like CMAKE\_C\_FLAGS to tune compilation.

* CMAKE\_INSTALL\_PREFIX to install into paths and so on. Use on the command 
  line through -DCMAKE\_INSTALL\_PREFIX=/my/path .
