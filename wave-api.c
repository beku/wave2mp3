/** @file wave-api.c
 *
 *  WindTone is a by the way recording utility for musicians.
 *
 *  @par Copyright:
 *            2018 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2018/03/02
 *
 *  basic RIFF Wave audio i/o
 */

/* cc -Wall -Wextra -g wave-api.c -o wave-test && ./wave-test /tmp/WindTone-0.wav */

#ifndef WAVE_API_H_
#define WAVE_API_H_

/* --- declarations --- */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* prepare i18n */
#ifndef _
#define _(x) x
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** simple Riff Wave PCM header */
typedef struct RiffWave_s {
  char     riff_mark[4];  /**< file signature 'RIFF' */
  uint32_t file_size;     /**< file size -8 byte */
  char     wave_mark[4];  /**< 'WAVE' marker */
  char     fmt_mark[4];   /**< 'fmt ' section */
  uint32_t fmt_size;      /**< fmt size : use 16 bytes */
  uint16_t fmt_type;      /**< 1 - PCM : uncompressed Pulse Code Modulation */
  uint16_t channels;      /**< number of channels : 1 - mono, 2 - stereo */
  uint32_t sample_rate;   /**< number of samples per second in Hz : e.g. 44000 Hz for CD */
  uint32_t frame_size;    /**< bytes per second */
  uint16_t block_align;   /**< channels · ((<bits/sample (one channels)> + 7) / 8) */
  uint16_t data_bits;     /**< number of data bits per sample and per channel - e.g. 16 bit for CD quality -> implicitely expects signed values */
  char     data_mark[4];  /**< 'data' section */
  uint32_t data_size;     /**< size of following samples bytes; max file size -44 byte */
} RiffWave_t;

int  isWave           ( RiffWave_t * header );
int  checkWave        ( RiffWave_t * header );
void getWavePlayTime  ( RiffWave_t * header,
                        int        * hours,
                        int        * minutes,
                        int        * seconds,
                        int        * remainder );
size_t getWaveOffset  ( RiffWave_t * header,
                        double       seconds );
const char * printWaveHeader( RiffWave_t * wave );
void setWaveHeader    ( RiffWave_t * header,
                        int          data_size,
                        int          sample_rate,
                        int          channel_count,
                        int          bit_rate );

#ifndef WAVE_API_SKIP_DEFINITION
/* --- definitions --- */

int  isWave           ( RiffWave_t * header )
{
  if( memcmp(header->riff_mark,"RIFF",4) == 0 &&
      memcmp(header->wave_mark,"WAVE",4) == 0 &&
      memcmp(header->fmt_mark,"fmt ",4) == 0
    )
    return 1;
  return 0;
}

int  checkWave        ( RiffWave_t * header )
{
  if(header->data_bits != 0 &&
     header->data_bits < 65535 &&
     header->channels > 0 &&
     header->channels < 65535 &&
     header->sample_rate >= 8000 &&
     header->sample_rate < 2147483647)
  {
    if(header->data_size > 0)
        return 0;
    else
        return -1;
  }
  return 1;
}

void getWavePlayTime ( RiffWave_t * header,
                       int        * hours,
                       int        * minutes,
                       int        * seconds,
                       int        * remainder )
{
  int byte_per_second = header->frame_size;
  int bps = (byte_per_second!=0) ? byte_per_second : 1;
  int seconds_ = header->data_size / bps;
  *hours = seconds_ / 3600;
  *minutes = seconds_ / 60 - *hours*60;
  *seconds = seconds_ - *hours*3600 - *minutes*60;
  *remainder = (header->data_size - seconds_ * bps) * 1000 / bps;
}
size_t getWaveOffset ( RiffWave_t * header,
                       double       seconds )
{
    int samples_per_second = header->sample_rate;
    int byte_per_sample = header->channels * header->data_bits/8;
    int frames = seconds * samples_per_second;
    int bytes = byte_per_sample * frames + 44;
    return bytes;
}

const char * printWaveHeader( RiffWave_t * wave )
{
  static char * t = NULL;
  if(!t) t = (char*) calloc(sizeof(char), 1024);
  if(!t) return _("can not allocate 1024 bytes in printWaveHeader()");

  t[0] = 0;

  if(memcmp(wave->wave_mark,"WAVE",4) != 0)
  {
    sprintf( &t[strlen(t)], _("no WAVE file") );
    return t;
  }
  sprintf(&t[strlen(t)], "%s\n", _("Wave format"));

  if(memcmp(wave->fmt_mark,"fmt ",4) != 0)
  {
    sprintf( &t[strlen(t)], _("no fmt section") );
    return t;
  }
  sprintf(&t[strlen(t)], "'fmt ' %s\n", _("Header"));
  sprintf(&t[strlen(t)], "%s:\t%d\n",
           _("Header Length"), wave->fmt_size);
  sprintf( &t[strlen(t)], "%s:\t\t%s\n%s:\t%d\n%s:\t%d\n%s / s:\t%d\n",
           _("Format"),       wave->fmt_type==1?_("uncompressed Pulse Code Modulation (PCM)"):_("unknown"),
           _("Channels"),     wave->channels,
           _("Sample Rate"),  wave->sample_rate,
           _("Bytes"),        wave->frame_size);
  sprintf( &t[strlen(t)], "%s:\t%d\n%s:\t%d\n",
           _("Block Align"),  wave->block_align,
           _("Data Bits"),    wave->data_bits );
  sprintf( &t[strlen(t)], "%s:\t%d\n",
           _("Data Size"),    wave->data_size );
  {
    int h,m,s,r;
    getWavePlayTime ( wave, &h, &m, &s, &r );
    sprintf( &t[strlen(t)], "%s:\t%02d:%02d:%02d.%02d\n",
           _("Play Time"),    h,m,s,r/10 );
  }

  return t;
}

void setWaveHeader ( RiffWave_t * header,
                     int          data_size,
                     int          sample_rate,
                     int          channel_count,
                     int          bit_rate )
{
  memset( header, 1, sizeof(RiffWave_t));

  memcpy( header->riff_mark,"RIFF",4);
  header->file_size = data_size + 44 - 8;
  memcpy( header->wave_mark,"WAVE",4);
  memcpy( header->fmt_mark,"fmt ",4);
  header->fmt_size = 16;
  header->fmt_type = 1; // PCM
  header->channels = channel_count;
  header->sample_rate = sample_rate;
  header->frame_size = sample_rate * channel_count * bit_rate / 8;
  header->block_align = channel_count * bit_rate / 8;
  header->data_bits = bit_rate;
  memcpy( header->data_mark,"data",4);
  header->data_size = data_size;
}
#endif // WAVE_API_SKIP_DEFINITION

#ifndef WAVE_API_SKIP_MAIN
int main( int argc, char**argv)
{
  FILE * fp = fopen(argv[1],"rb");
  RiffWave_t * wave = NULL;
  size_t r;

  if(!fp || argc < 2)
  {
    puts("Benutzung: wave-test Dateiname.wav\n");
    exit(0);
  }

  char block[1024];
  memset(block,0,1024);
  wave = (RiffWave_t*) &block;
  r = fread( &block, sizeof(char), 1024, fp );
  if(r > 44 && !isWave(wave))
    puts("no Wave file");

  if(memcmp(block,"RIFF",4) != 0)
  {
    puts("no Riff file");
    fclose(fp);
    exit(0);
  }
  puts("Riff intel");

  fseek(fp,0L,SEEK_END); 
  int sz = ftell (fp);
  if(sz == -1)
  {
    puts("can not read file\n");
    fclose(fp);
    exit(0);
  }
  int length = *((uint32_t*)&block[4]);
  printf("file size:\t%d\ndata size:\t%d bytes\n", sz, length );

  if(memcmp(wave->wave_mark,"WAVE",4) != 0)
  {
    puts("no WAVE file");
    fclose(fp);
    exit(0);
  }

  if(memcmp(wave->fmt_mark,"fmt ",4) != 0)
  {
    puts("no fmt section");
    fclose(fp);
    exit(0);
  }
  fclose(fp);

  puts( printWaveHeader( wave ) );

  return 0;
}
#endif // WAVE_API_SKIP_MAIN

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif // WAVE_API_H_
