TEMPLATE = app

QT += multimedia quick qml svg
QTPLUGIN += qsvg
CONFIG += STATIC_LIB_ARGS # DYNAMIC_LIB_ARGS # STATIC_LIB_ARGS

android {
  QT += androidextras
  equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/oyjl/
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/lame/
    LIBS += -L/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/lib/
  }
  equals(ANDROID_TARGET_ARCH, x86) {
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/x86-linux-androideabi-4.9/include/
    LIBS += -L/run/media/kuwe/KAI-DA/linux/x86-linux-androideabi-4.9/lib/
  }
  QMAKE_LFLAGS+=-fopenmp
  LIBS += -lm
}
unix:!macx:!android {
QT += dbus
INCLUDEPATH+=/home/kuwe/.local/include/
INCLUDEPATH+=/home/kuwe/.local/include/oyjl
INCLUDEPATH+=/home/kuwe/.local/include/lame
INCLUDEPATH+=/opt/local/include/
LIBS+=-L/home/kuwe/.local/lib64
LIBS+=-L/opt/local/lib64
}

LIBS += -lmp3lame-static

DYNAMIC_LIB_ARGS { # dlopen libOyjlArgsQml
  LIBS += -lOyjlArgsQml
  LIBS += -lOyjl
  LIBS += -lOyjlCore
} else:STATIC_LIB_ARGS { # link statically liboyjl-args-qml-static
  DEFINES += COMPILE_STATIC
  LIBS += -loyjl-args-qml-static
  LIBS += -loyjl-static
  unix:!macx:!android {
    LIBS += -lopenicc-static
    LIBS += -lxml2
    LIBS += -lyaml
    LIBS += -lyajl
  } else {
    LIBS += -lyajl-static
    LIBS += -lxml2-static
  }
  LIBS += -loyjl-core-static
}

LIBS += -ldl


INCLUDEPATH += .
SOURCES +=   wave2mp3.c

lupdate_only{
    SOURCES += dummy.qml
}

# Extra resources
RESOURCES += wave2mp3.qrc

# Default rules for deployment.
#include(deployment.pri)

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml \
    android/src/org/oyranos/testApp/AppActivity.java

DISTFILES += \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-ldpi/icon.png \
    android/res/drawable-mdpi/icon.png \
